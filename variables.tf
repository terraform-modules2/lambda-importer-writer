##########################
# Writer Stage Variables #
##########################
variable "wrtie__function_name" {type = string}
variable "write__environment" {}
variable "write__filename" {type = string}
variable "write__function_timeout" {type = number}
variable "write__handler" {type = string}
variable "write__runtime" {type = string}
variable "write__security_group_ids" {type = list(string)}
variable "write__source_code_hash" {}
variable "write__subnet_ids" {type = list(string)}
variable "write__memory_size" {
  default = 128
  type = number
}