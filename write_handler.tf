resource "aws_lambda_function" "import__writer" {
  function_name = var.wrtie__function_name
  handler = var.write__handler
  role = aws_iam_role.lambda-import-writer.arn
  runtime = var.write__runtime


  source_code_hash = var.write__source_code_hash
  filename = var.write__filename

  environment = var.write__environment

  vpc_config {
    security_group_ids = var.write__security_group_ids
    subnet_ids = var.write__subnet_ids
  }

  timeout = var.write__function_timeout
  memory_size = var.write__memory_size
}